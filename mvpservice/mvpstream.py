import datetime
import pyspark_cassandra
import pyspark_cassandra.streaming

from pyspark_cassandra import CassandraSparkContext

from pyspark.sql import SQLContext
from pyspark import SparkContext, SparkConf
from pyspark.streaming import StreamingContext
from pyspark.streaming.kafka import KafkaUtils

import json

def load_event(eventobj):
    """Load an event object, which is 1-1 except timestamp"""
    print(dir(eventobj))
    event = json.loads(eventobj)
    event['event_time'] = datetime.datetime.fromtimestamp(event['event_time'])
    return event

conf = SparkConf() \
    .setAppName("mvpservice") \
    .setMaster("spark://sparkmaster:7077") \
    .set("spark.cassandra.connection.host", "cassandra")

# set up our contexts
sc = CassandraSparkContext(conf=conf)
sql = SQLContext(sc)
stream = StreamingContext(sc, 1) # 1 second window

kafka_stream = KafkaUtils.createStream(stream, \
                                       'kafka', \
                                       "raw-event-streaming-consumer",
                                       {"mvpservice":1})

# Default result -- passthrough
parsed = kafka_stream.map(lambda x: load_event(x[1]))
# Pretty print for debugging
parsed.pprint()

# Record the event
parsed.saveToCassandra("mvpservice", "event")

# Abnormally high heart rate (> 190)
high_heart_rate = parsed.filter(lambda event: event['heart_rate'] > 190)
print('High Heart Rate!!!')
high_heart_rate.pprint()
alert_entries = high_heart_rate.map(lambda x: {
    'user_id': x.get('user_id'),
    'alert_time': x.get('event_time'),
    'message': 'High heart rate (> 190)'
})
alert_entries.saveToCassandra("mvpservice", "alert")

# # Low blood pressure (systolic < 100) and high heart rate (> 100)
low_bp_high_hr = parsed.filter(lambda event: (event['heart_rate'] > 100
                                              and event['systolic'] < 100))
print('Low BP and High Heart Rate!!!')
low_bp_high_hr.pprint()
alert_entries = high_heart_rate.map(lambda x: {
    'user_id': x.get('user_id'),
    'alert_time': x.get('event_time'),
    'message': 'Low BP (< 100) and High heart rate (> 100)'
})
alert_entries.saveToCassandra("mvpservice", "alert")


stream.start()
stream.awaitTermination()
