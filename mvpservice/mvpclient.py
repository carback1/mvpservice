from __future__ import print_function

import grpc
import numpy
import time
import json

from scipy.stats import truncnorm


from mvpservice_generated import mvpservice_pb2_grpc
from mvpservice_generated import mvpservice_pb2


def get_normal_value_generator(midpoint, minvalue, maxvalue, deviation):
    """Given a midpoint, max, and min, generate a value using a
    gaussian (normal) value with the given standard deviation

    note that numpy's truncnorm is complicated, so this really exists
    to make it human-accessible
    """
    assert minvalue > 0 and maxvalue > 0 and midpoint > 0
    assert minvalue < maxvalue
    assert midpoint > minvalue and midpoint < maxvalue
    return truncnorm((minvalue - midpoint)/deviation,
                     (maxvalue - midpoint)/deviation,
                     loc=midpoint,
                     scale=deviation)

def generate_random_event(generators):
    entry = { k: int(v.rvs())
              for k, v in generators.items() }
    event = mvpservice_pb2.Event(user_id=entry.get('user_id'),
                                 heart_rate=entry.get('heart_rate'),
                                 systolic=entry.get('systolic'),
                                 diastolic=entry.get('diastolic'))
    # print('{}'.format(str(event)))
    yield event

def run():
  channel = grpc.insecure_channel('mvpservice:50051')
  stub = mvpservice_pb2_grpc.MvpServiceStub(channel)
  seed = 12345
  numpy.random.seed(seed=seed)
  generators = {
      "user_id": get_normal_value_generator(10000, 1, 100000, 5000),
      "heart_rate": get_normal_value_generator(50, 1, 400, 50),
      "systolic": get_normal_value_generator(120, 10, 300, 20),
      "diastolic": get_normal_value_generator(80, 1, 200, 15)
  }

  cnt = 0
  while True:
      summary = stub.RecordEvent(generate_random_event(generators))
      cnt += 1
      if cnt % 1000 == 0:
          print('Event Count so far: {}'.format(cnt))
