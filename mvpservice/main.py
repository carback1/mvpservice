"""
main.py - main functionality for tsgen tool
"""

import click
click.disable_unicode_literals_warning = True

import mvpservice.mvpserver
import mvpservice.mvpclient

@click.group()
def cli():
    pass

@cli.command()
def server():
    mvpservice.mvpserver.serve()

@cli.command()
def client():
    mvpservice.mvpclient.run()

if __name__ == '__main__':
    cli()
