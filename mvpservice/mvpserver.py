from concurrent import futures
import time
import datetime
import calendar
import json

import grpc

from kafka import KafkaClient, SimpleProducer

from mvpservice_generated import mvpservice_pb2_grpc
from mvpservice_generated import mvpservice_pb2

_ONE_DAY_IN_SECONDS = 60 * 60 * 24

class MvpServiceServicer(mvpservice_pb2_grpc.MvpServiceServicer):
    def __init__(self):
        kafka = KafkaClient("kafka:9092") # TODO: Parameterize
        self.producer = SimpleProducer(kafka)

    def RecordEvent(self, request_iter, context):
        print("Record Event")
        cnt = 0
        for event in request_iter:
            ts = calendar.timegm(datetime.datetime.utcnow().timetuple())
            message = {
                'user_id': event.user_id,
                'heart_rate': event.heart_rate,
                'systolic': event.systolic,
                'diastolic': event.diastolic,
                'event_time': ts
            }
            print('Sending to kafka: {}'.format(message))
            self.producer.send_messages(u'mvpservice',
                                        bytes(json.dumps(message), u'ascii'))
            cnt += 1
        return mvpservice_pb2.EventSummary(count=cnt)

    def GetAlerts(self, request, context):
        print("Hello, Alerts!")
        return [mvpservice_pb2.Alert(user_id=12345,
                                     message="Hello, alertmessage")]

    def getHelloWorld(self, request, context):
        print("Helloworld!")
        return mvpservice_pb2.BLAH(blah="Blah!")

def serve():
  server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))
  mvpservice_pb2_grpc.add_MvpServiceServicer_to_server(
      MvpServiceServicer(), server)
  server.add_insecure_port('[::]:50051')
  server.start()
  try:
    while True:
      time.sleep(_ONE_DAY_IN_SECONDS)
  except KeyboardInterrupt:
      server.stop(0)
