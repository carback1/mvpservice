Service that provides time series generation of the format:

```
{
"user_id": 12345,
"heart_rate": 100,
"systolic": 120,
"diastolic": 80
}
```

All values are ints. Service is provided using gRPC and processing is
done with spark streams.

Regnerate service defs with:

```
python -m grpc_tools.protoc -I. --python_out=./mvpservice_generated/ \
  --grpc_python_out=./mvpservice_generated/ mvpservice.proto
```

= Usage =

```
python setup.py --user install # Install to local user
python -mwellframe.mvpservice --help
```

There are 3 commands: server, client, and stream. These are called
based on what role the caller is performing:

1. server: a gRPC service that provides an endpoint for clients and
   acts as a kafka producer for spark streaming jobs.
2. client: A gRPC client that can send events and read alerts from the server.
3. stream: A spark stream consumer, typically invoked via pyspark submit.

= Other Notes =

The pyspark and pyspark-cassandra dependencies are a little complicated and not
documented well:

- `pip install pyspark` will give you the bulk of what you need, and
  you should preferably do this in a virtualenv and not as root
- You can run the stream examples in a python shell with:

```
pyspark --packages anguenot/pyspark-cassandra:0.4.0,\
  org.apache.spark:spark-streaming-kafka-0-8_2.11:2.2.0 \
  --conf spark.cassandra.connection.host=localhost
```

Officially, the disrtributed pyspark does not run on python older than
2.7. Unofficially it seems to work without issues.

The server and stream commands need kafka to work. The stream needs
pyspark to work. These both need their respective servers installed
and running to do useful work (cassandra, kafka, pyspark).

Because of the complexities here and the lack of deployment
infrastructure, I decided to keep all three commands as one component
instead of breaking them up. Typically I would want to break out the
data models and protocols into their own projects and have everything
depend on that. Unit tests are similarly complicated but the lack of
polish there is a lack of time.
