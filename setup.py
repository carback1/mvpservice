"""setup.py -- setup script

Setuptools config
"""

from setuptools import setup


setup(
    name='wellframe-mvpservice',
    version='0.0.0',
    packages=['mvpservice', 'mvpservice_generated'],
    install_requires=[
        'numpy',
        'scipy',
        'kafka',
        'pyspark',
        'grpcio-tools',
        'pytest-cov',
        'pytest',
        'coverage',
        'click'
    ],
    entry_points={
       'console_scripts': [
           'mvpservice = mvpservice.main:cli'
       ]
    }
)
