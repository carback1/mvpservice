"""
test_main.py -- smoke test
"""
import pytest

from click.testing import CliRunner

from mvpservice import main


@pytest.fixture(scope='module')
def runner():
    return CliRunner()

def test_main(runner):
    result = runner.invoke(main.cli, ['--help'])
    assert result.exit_code == 0
